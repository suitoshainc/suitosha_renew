<?php defined('C5_EXECUTE') or die(_("Access Denied."));
use Concrete\Package\SuitonBaseUtil\Src\Express\Entry\Exchange\EntryTreeNodeList;
$au = Core::make('helper/aUtil');
$uh = Core::make('helper/url');
$nh = Core::make('helper/navigation');

$c = Page::getCurrentPage();
if ($entity) { 
        $entityManager = Core::make('database/orm')->entityManager();
        $entity = $entityManager->find('Concrete\Core\Entity\Express\Entity', 'eeceee62-36e0-11e7-b8bc-b083fec8c1b7');
        $entrytree = new EntryTreeNodeList($entity,'portfolio_tag_parent');
    ?>
    <h1>複合検索</h1>
    <form action="" method="GET">
    <?php 
        echo '<ul>';
        $entrytree->renderTree();
        echo '</ul>';
    ?>
    <input type="submit" value="検索">
    </form>
    <?php 
    $results = $result->getItemListObject()->getResults();
    if (count($results)) { ?>
        <?php foreach($result->getItems() as $item):?>
            <?php
                $entry = $item->getEntry();
                $portfolio_title = $entry->getAttributeValueObject('portfolio_title');
                $portfolio_date = $entry->getAttributeValueObject('portfolio_date');
                $portfolio_mainimg = $entry->getAttributeValueObject('portfolio_mainimg');
                $portfolio_url = $entry->getAttributeValueObject('portfolio_url');
                $portfolio_category = $entry->getAttributeValueObject('portfolio_category');
                $portfolio_tags = $entry->getPortfolioTags('portfolio_tags');
                $portfolio_relate_staff = $entry->getAttributeValueObject('portfolio_relate_staff');

                $link = URL::to('portfolio/detail/',$entry->getID());
            ?>
                <h1><a href="<?php echo $link;?>"><?php echo $portfolio_title;?></a></h1>
                <aside>
                <?php
                if($portfolio_tags){

                    echo '<ul>';
                    foreach($portfolio_tags as $tag){
                        $params = array(
                            'category[]' => $tag->getID()
                        );

                        $url = $uh->buildQuery($nh->getLinkToCollection($c), $params);
                        echo '<li><a href="'.$url.'">'.$tag->getPortfolioTagTitle().'</a></li>';
                    }
                    echo '</ul>';
                }
                ?>
                <?php echo $portfolio_date;?>
                </aside>
                <?php
                $main_img = $portfolio_mainimg->getValue();
                if(is_object($main_img)){
                    echo '<img src="'.$au->thumb_src($main_img,'port_folio').'" alt="">';
                }

                if($portfolio_url){
                    echo '<p><a href="'.$portfolio_url .'" target="_blank">'.$portfolio_url .'</a></p>';
                }

                if($portfolio_relate_staff){
                    $staffid = $portfolio_relate_staff->getValue();
                    $staffid = explode(',', $staffid);
                    echo '<ul>';
                    foreach($staffid as $uid){
                        if($uid !== 1){
                            $ui = UserInfo::getByID($uid);
                            echo '<li>'.$ui->getAttribute('data_display_name').'</li>';
                        }
                    }
                    echo '</ul>';
                }
            ?>
        <?php endforeach;?>

        <?php if ($pagination) { ?>
            <?php echo $pagination ?>
        <?php } ?>

        <script type="text/javascript">
            $(function() {
                $.concreteExpressEntryList({
                    'bID': '<?php echo $bID?>'
                });
            });
        </script>

    <?php } else { ?>

        <p><?php echo t('No "%s" entries can be found', $entity->getName())?>

    <?php } ?>


<?php } ?>