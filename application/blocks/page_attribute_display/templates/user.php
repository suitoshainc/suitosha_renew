<?php
defined('C5_EXECUTE') or die('Access Denied.');
$au = Core::make('helper/aUtil');
$c = \Page::getCurrentPage();
$user_id = $c->getCollectionUserID();
$ui = UserInfo::getByID($user_id);
$data_display_name = $ui->getAttribute('data_display_name');
$data_user_position = $ui->getAttribute('data_user_position');
$data_user_profile = $ui->getAttribute('data_user_profile');
$data_user_thumbnail = $ui->getAttribute('data_user_thumbnail');
$user_thumbnail_src = $au->thumb_src($data_user_thumbnail);
?>
<section class="user_info">
	<div class="img">
		<img src="<?php echo $user_thumbnail_src;?>" alt="<?php echo $data_display_name;?>" width="200">
	</div>
	<div class="conent">
		<h4 class="ttl"><?php echo $data_display_name;?></h4>
		<p class="pos"><?php echo $data_user_position;?></p>
		<div class="prof"><?php echo $data_user_profile;?></div>
	</div>
	
</section>