<?php
namespace Concrete\Package\UserSelectorAttribute\Attribute\UserSelector;

defined('C5_EXECUTE') or die("Access Denied.");
use Core;
use UserInfo;
use Concrete\Core\Entity\Attribute\Value\Value\TextValue;

class Controller extends \Concrete\Core\Attribute\DefaultController
{
	public function form() {
		$seleced_value = [];
		if (is_object($this->attributeValue)) {
			$seleced_value = $this->getAttributeValue()->getValue();
			$seleced_value = explode(',', $seleced_value);
		}
		$list = new \Concrete\Core\User\UserList();
		$users = $list->getResults();
		foreach($users as $u){
			$uid = $u->getUserID();
			if($uid !== 1){
				$ui = UserInfo::getByID($uid);
				$select_array[$uid] = $ui->getAttribute('data_display_name');
			}
		}
		$this->set('selects',$select_array);
		$this->set('selected',$seleced_value);
	}

	public function createAttributeValue($value)
	{
		if(is_array($value)){
			$value = implode(',', $value);
		}
		$av = new TextValue();
		$av->setValue($value);

		return $av;
	}

}