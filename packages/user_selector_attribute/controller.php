<?php 
namespace Concrete\Package\UserSelectorAttribute;

use Concrete\Core\Package\Package;
use Concrete\Core\Backup\ContentImporter;

defined('C5_EXECUTE') or die("Access Denied.");

class Controller extends \Concrete\Core\Package\Package {

	protected $pkgHandle = 'user_selector_attribute';
	protected $appVersionRequired = '5.8.0';
	protected $pkgVersion = '1.0';
	
	public function getPackageDescription() {
		return t("Attribute that allows the selection of users.");
	}
	
	public function getPackageName() {
		return t("User Selector Attribute");
	}
	
	public function install() {
		$pkg = parent::install();
		$ci = new ContentImporter();
		$ci->importContentFile($pkg->getPackagePath() . '/config/install.xml');
	}
}