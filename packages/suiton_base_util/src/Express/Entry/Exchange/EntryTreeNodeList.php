<?php
namespace Concrete\Package\SuitonBaseUtil\Src\Express\Entry\Exchange;

use Concrete\Core\Entity\Express\Entity;
use Concrete\Core\Express\EntryList;
use Concrete\Core\Support\Facade\Express;
use Concrete\Package\SuitonBaseUtil\Src\Express\Entry\Exchange\EntryTree;

class EntryTreeNodeList extends EntryTree
{
	public $depth = 0;

	public function renderTree($renderRecursive = 'renderRecursive'){
		foreach($this->tree as $tree){
			$this->$renderRecursive($tree);
		}
	}

	protected function renderRecursive($array){
		if($array){
			if($array['parent_id'] == 0){
				$this->depth = 0;
			}

			if(isset($_GET['category']) && count($_GET['category']) > 0){
				if(in_array($array['id'],$_GET['category'])){
					$checked = ' checked="checked"';
				}else{
					$checked = '';
				}
			}

			$entry = Express::getEntry($array['id']);
			echo '<li><label><input type="checkbox"'.$checked.' name="category[]" value="'.$array['id'].'">&nbsp;'.$entry->getPortfolioTagTitle().'</label>';
			if($array['children']){
				$this->depth++;
				echo '<ul class="depth-'.$this->depth.'">';
				foreach($array['children'] as $child){
					$this->renderRecursive($child);
				}
				echo '</ul>';
			}
			echo '</li>';
		}
	}

	protected function renderRecursiveSimple($array){
		if($array){
			if($array['parent_id'] == 0){
				$this->depth = 0;
			}

			$entry = Express::getEntry($array['id']);
			echo '<li>'.$entry->getPortfolioTagTitle();
			if($array['children']){
				$this->depth++;
				echo '<ul class="depth-'.$this->depth.'">';
				foreach($array['children'] as $child){
					$this->renderRecursiveSimple($child);
				}
				echo '</ul>';
			}
			echo '</li>';
		}
	}
}
