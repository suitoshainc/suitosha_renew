<?php
namespace Concrete\Package\SuitonBaseUtil\Src\Express\Entry\Exchange;

define('DIR_BASE', '/Applications/MAMP/htdocs/suitosha_renew/');

require '/Applications/MAMP/htdocs/suitosha_renew/concrete/bootstrap/configure.php';
require '/Applications/MAMP/htdocs/suitosha_renew/concrete/bootstrap/autoload.php';
require '/Applications/MAMP/htdocs/suitosha_renew/concrete/bootstrap/start.php';

use \Concrete\Core\Entity\Express\Entity as Entity;
use \Concrete\Core\Express\EntryList as EntryList;
use \Core;

class EntryTreeTest extends \PHPUnit_Framework_TestCase
{
	public $entryList;
	public $parent_attr;
	public $entryTree;
	public $tree;

	 /**
     * これが各テストメソッドの前に実行されるメソッドになります。
     * ここで対象クラスのnewと依存オブジェクトの用意をしましょう。
     * このテストクラスを見た人が対象クラスの使い方が分かり易くなりますね
     */
	public function setUp()
	{
		//対象クラスはリアルなオブジェクトです
		$entityManager = \Core::make('database/orm')->entityManager();
		$entity = $entityManager->find('Concrete\Core\Entity\Express\Entity', 'eeceee62-36e0-11e7-b8bc-b083fec8c1b7');

		$this->entryList = new EntryList($entity);
		$this->parent_attr = 'parent_id';
	}

	/**
     * testから始まるメソッド名がテストメソッドです。
     * assertは基本、左側がexpected(期待値), 右側がactual(実際の値)
     * assertInstanceOfは、actualがexpectedに指定した型に合っているかをチェックします。
     * 合っていれば成功です。
     */
	public function testInstance()
	{
		$this->assertInstanceOf('\Concrete\Core\Express\EntryList', $this->entryList);
	}

	// public function __construct(Entity $entity,$parent_attr = 'parent_id'){
	// 	$this->entryList = new EntryList($entity);
	// 	$this->parent_attr = $parent_attr;
	// 	$this->getTree();
	// }

	/**
	 * entityからentryを生成して配列に変換
	 *
	 * @return array
	 */
	protected function exchangeEntryToArray(EntryList $entryList){
		$this->entryList = $entryList->getResults();
		$entryArrayExchangeList = [];
		foreach ($this->entryList as $entry) {
			$tmp = [];
			$tmp['id'] = $entry->getID();
			$parent_attr_value = $entry->getAttributeValueObject($this->parent_attr);
			if(is_object($parent_attr_value)){
				$parent_entry = $parent_attr_value->getValueObject()->getSelectedEntries()[0];
				if($parent_entry instanceof \Concrete\Core\Entity\Express\Entry){
					$tmp['parent_id'] = $parent_entry->getID();
				}else{
					$tmp['parent_id'] = 0;
				}
			}else{
				$tmp['parent_id'] = 0;
			}

			$entryArrayExchangeList[] = $tmp;
		}
		return $entryArrayExchangeList;
	}

	/**
	 * entry listをツリー状に変形
	 * @return array
	 */
	protected function exchangeArrayToTree(array $list , $idField = 'id', $parentField = 'parent_id', $rootParantID = 0){
		$tree = array();
		$index = array();

		foreach ($list as $value) {
			$id = $value[$idField];
			$pid = $value[$parentField];

			if (isset($index[$id])) {
				$value['children'] = $index[$id]['children'];
				$index[$id] = $value;
			} else {
				$index[$id] = $value;
			}

			if ($pid == $rootParantID) {
				$tree[] = & $index[$id];
			} else {
				$index[$pid]["children"][] = & $index[$id];
			}
		}
		return $tree;
	}

	/**
	 * ツリー化されたentry listを返す
	 * @return mixed
	 */
	protected function getTree(){
		$entryArrayExchangeList = $this->exchangeEntryToArray($this->entryList);
		if(is_array($entryArrayExchangeList)){
			$this->tree = $this->exchangeArrayToTree($entryArrayExchangeList);
		}
	}

	//abstract public function renderTree();
}
