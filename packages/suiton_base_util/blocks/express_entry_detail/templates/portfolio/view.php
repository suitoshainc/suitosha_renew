<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php
$au = Core::make('helper/aUtil');
if (isset($entry) && is_object($entry)) { ?>
<?php
$portfolio_title = $entry->getAttributeValueObject('portfolio_title');
$portfolio_date = $entry->getAttributeValueObject('portfolio_date');
$portfolio_mainimg = $entry->getAttributeValueObject('portfolio_mainimg');
$portfolio_url = $entry->getAttributeValueObject('portfolio_url');
$portfolio_category = $entry->getAttributeValueObject('portfolio_category');
$portfolio_tags = $entry->getPortfolioTags('portfolio_tags');
$portfolio_relate_staff = $entry->getAttributeValueObject('portfolio_relate_staff');
?>
<h1><?php echo $portfolio_title;?></h1>
<aside>
<?php
if($portfolio_tags){
	echo '<ul>';
	foreach($portfolio_tags as $tag){
		echo '<li><a href="'.URL::to('portfolio/',$tag->getID()).'">'.$tag->getPortfolioTagTitle().'</a></li>';
	}
	echo '</ul>';
}
?>
<?php echo $portfolio_date;?>
</aside>
<?php
$main_img = $portfolio_mainimg->getValue();
if(is_object($main_img)){
	echo '<img src="'.$au->thumb_src($main_img,'port_folio').'" alt="">';
}

if($portfolio_url){
	echo '<p><a href="'.$portfolio_url .'" target="_blank">'.$portfolio_url .'</a></p>';
}

if($portfolio_relate_staff){
	$staffid = $portfolio_relate_staff->getValue();
	$staffid = explode(',', $staffid);
	echo '<ul>';
	foreach($staffid as $uid){
		if($uid !== 1){
			$ui = UserInfo::getByID($uid);
			echo '<li>'.$ui->getAttribute('data_display_name').'</li>';
		}
	}
	echo '</ul>';
}
?>
<?php } ?>
