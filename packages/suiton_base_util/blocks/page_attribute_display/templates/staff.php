<?php

defined('C5_EXECUTE') or die('Access Denied.');
$au = Core::make('helper/aUtil');
$list = new \Concrete\Core\User\UserList();
$users = $list->getResults();
if($users){
	foreach($users as $u){
		$uid = $u->getUserID();
		if($uid !== 1){
			$ui = UserInfo::getByID($uid);
			$data_display_name = $ui->getAttribute('data_display_name');
			$data_user_position = $ui->getAttribute('data_user_position');
			$data_user_profile = $ui->getAttribute('data_user_profile');
			$data_user_thumbnail = $ui->getAttribute('data_user_thumbnail');
			$user_thumbnail_src = $au->thumb_src($data_user_thumbnail);
		}
?>
	<section class="user_info">
		<a href="<?php echo \URL::to('staff/detail',$uid);?>">
		<?php if($user_thumbnail_src):?>
		<div class="img">
			<img src="<?php echo $user_thumbnail_src;?>" alt="<?php echo $data_display_name;?>" width="200">
		</div>
		<?php endif;?>
		<div class="conent">
			<h4 class="ttl"><?php echo $data_display_name;?></h4>
			<p class="pos"><?php echo $data_user_position;?></p>
			<div class="prof"><?php echo $data_user_profile;?></div>
		</div>
		</a>
	</section>
<?php }}?>
