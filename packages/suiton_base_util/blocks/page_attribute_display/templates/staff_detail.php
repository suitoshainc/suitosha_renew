<?php
defined('C5_EXECUTE') or die('Access Denied.');
use Concrete\Core\Entity\Express\Entity;
use Concrete\Core\Express\EntryList;

$uh = Core::make('helper/url');
$nh = Core::make('helper/navigation');
$au = Core::make('helper/aUtil');

$c = \Page::getCurrentPage();
if(isset($_GET['custom_route_id']) &&  is_numeric($_GET['custom_route_id']) && $_GET['custom_route_id'] > 1):
	$user_id = $_GET['custom_route_id'];
	$ui = UserInfo::getByID($user_id);
	$data_display_name = $ui->getAttribute('data_display_name');
	$data_user_position = $ui->getAttribute('data_user_position');
	$data_user_profile = $ui->getAttribute('data_user_profile');
	$data_user_thumbnail = $ui->getAttribute('data_user_thumbnail');
	$user_thumbnail_src = $au->thumb_src($data_user_thumbnail);

	$account_profile_links = $ui->getAttribute('account_profile_links');
	$html .= '<span class="ccm-social-link-attribute-display">';
	foreach ($account_profile_links->getSelectedLinks() as $link) {
		$serviceObject = \Concrete\Core\Sharing\SocialNetwork\Service::getByHandle($link->getService());
		if (is_object($serviceObject)) {
			$iconHtml = $serviceObject->getServiceIconHTML();
		}
		$html .= '<span class="ccm-social-link-service">';
		$html .= '<span class="ccm-social-link-service-icon"><a target="_blank" href="' . filter_var($link->getServiceInfo(),
				FILTER_VALIDATE_URL) . '">' . $iconHtml . '</a></span>';
		$html .= '<span class="ccm-social-link-service-info"><a target="_blank" href="' . filter_var($link->getServiceInfo(),
				FILTER_VALIDATE_URL) . '">' . $serviceObject->getName() . '</a></span>';
		$html .= '</span>';
	}
	$html .= '</span>';


	$entity_id = '99bd39f8-36de-11e7-b8bc-b083fec8c1b7';
	$entityManager = Core::make('database/orm')->entityManager();
	$entity = $entityManager->find('Concrete\Core\Entity\Express\Entity', $entity_id);

	$entryList = new EntryList($entity);
	if($user_id){
		$entryList->filter(false, "(FIND_IN_SET('".$user_id."', ak_portfolio_relate_staff))");
	}
	$result = $entryList->getResults();
?>
	<section class="user_info">
		<div class="img">
			<img src="<?php echo $user_thumbnail_src;?>" alt="<?php echo $data_display_name;?>" width="200">
		</div>
		<div class="conent">
			<h4 class="ttl"><?php echo $data_display_name;?></h4>
			<p class="pos"><?php echo $data_user_position;?></p>
			<div class="prof"><?php echo $data_user_profile;?></div>
			<div class="social"><?php echo $html;?></div>
		</div>
	</section>

	<section class="user_related_works">
		<header>
			<h2>仕事の実績</h2>
		</header>
		<?php if($result):?>
			<ul class="user_related_works__ul"></ul>
			<?php foreach($result as $entry):?>
				<li>
				<?php
					//$entry = $item->getEntry();
					$portfolio_title = $entry->getAttributeValueObject('portfolio_title');
					$portfolio_date = $entry->getAttributeValueObject('portfolio_date');
					$portfolio_mainimg = $entry->getAttributeValueObject('portfolio_mainimg');
					$portfolio_url = $entry->getAttributeValueObject('portfolio_url');
					$portfolio_category = $entry->getAttributeValueObject('portfolio_category');
					$portfolio_tags = $entry->getPortfolioTags('portfolio_tags');
					$portfolio_relate_staff = $entry->getAttributeValueObject('portfolio_relate_staff');

					$link = URL::to('portfolio/detail/',$entry->getID());
				?>
					<h1><a href="<?php echo $link;?>"><?php echo $portfolio_title;?></a></h1>
					<aside>
					<?php
					if($portfolio_tags){

						echo '<ul>';
						foreach($portfolio_tags as $tag){
							$params = array(
								'category[]' => $tag->getID()
							);

							$url = $uh->buildQuery($nh->getLinkToCollection($c), $params);
							echo '<li><a href="'.$url.'">'.$tag->getPortfolioTagTitle().'</a></li>';
						}
						echo '</ul>';
					}
					?>
					<?php echo $portfolio_date;?>
					</aside>
					<?php
					$main_img = $portfolio_mainimg->getValue();
					if(is_object($main_img)){
						echo '<img src="'.$au->thumb_src($main_img,'port_folio').'" alt="">';
					}

					if($portfolio_url){
						echo '<p><a href="'.$portfolio_url .'" target="_blank">'.$portfolio_url .'</a></p>';
					}

					// if($portfolio_relate_staff){
					// 	$staffid = $portfolio_relate_staff->getValue();
					// 	$staffid = explode(',', $staffid);
					// 	echo '<ul>';
					// 	foreach($staffid as $uid){
					// 		if($uid !== 1){
					// 			$ui = UserInfo::getByID($uid);
					// 			echo '<li>'.$ui->getAttribute('data_display_name').'</li>';
					// 		}
					// 	}
					// 	echo '</ul>';
					// }
					//
				?>
				</li>
			<?php endforeach;?>
			</ul>
		<?php endif;?>
	</section>

	<section class="user_recent_post">
		<header>
			<h2>最近書いた記事</h2>
		</header>
		<?php
			$list = new \Concrete\Core\Page\PageList();
			$list->filterByPageTypeHandle('type_blog');
			$list->filterByUserID($user_id);
			$pages = $list->getResults();
		?>
		<?php if($pages):?>
			<ul>
			<?php foreach($pages as $page):?>
				<li>
					<a href="<?php echo $nh->getLinkToCollection($page);?>"><?php echo h($page->getCollectionName());?></a>
				</li>
			<?php endforeach;?>
			</ul>
		<?php endif;?>
	</section>
<?php endif;?>
