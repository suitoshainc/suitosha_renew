<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php $this->inc('elements/header.php');?>
<div class="content_wrapper">
	<section class="main">
		<h1><?php echo h($c->getCollectionName());?></h1>
		<?php
			$a = new Area('Main');
			$a->display($c);
		?>
		<?php
			$a = new GlobalArea('Blog Footer');
			$a->display();
		?>
	</section>

	<aside class="side">
		<?php
			$a = new GlobalArea('Blog Sidebar');
			$a->display();
		?>
	</aside>
</div>
<?php $this->inc('elements/footer.php') ?>
