<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php $this->inc('elements/header.php');?>

<?php
if(isset($_GET['custom_route_id']) && $_GET['custom_route_id'] > 0 && is_numeric($_GET['custom_route_id'])){
	$a = new GlobalArea('Portfolio info');
	$a->setBlockLimit(1);//個数制限したいとき
	$a->display();

	$a = new Area('Main__portfolio--'.$_GET['custom_route_id']);
	$a->display($c);
}else{
	echo '表示できる制作事例がありません。';
}
?>

<?php
$a = new GlobalArea('Entry Footer');
$a->display();
?>

<?php $this->inc('elements/footer.php') ?>
