<?php
use Concrete\Package\SuitonBaseUtil\Src\Express\Entry\Exchange\EntryTreeNodeList;
defined('C5_EXECUTE') or die("Access Denied."); ?>

<div class="ccm-dashboard-content-full">

<?php if ($list->getTotalResults()) { ?>


        <div class="table-responsive">
            <?php View::element('express/entries/search', array('controller' => $searchController)) ?>
        </div>

    <script type="text/javascript">
        $(function() {
            ConcreteEvent.subscribe('SelectExpressEntry', function(e, data) {
                var url = '<?php echo $view->action('view_entry', 'ENTRY_ID')?>';
                url = url.replace('ENTRY_ID', data.exEntryID);
                if (data.event.metaKey) {
                    window.open(url);
                } else {
                    window.location.href = url;
                }
            });
        });
    </script>

    <?php
} else {
    ?>

    <div class="ccm-dashboard-content-full-inner">

    <p><?php echo t('None created yet.') ?></p>

        </div>

    <?php
} ?>

<?php
if(is_object($entity)){
	if($entity->getID() == 'eeceee62-36e0-11e7-b8bc-b083fec8c1b7'){
		$entityManager = Core::make('database/orm')->entityManager();
		$entity = $entityManager->find('Concrete\Core\Entity\Express\Entity', $entity->getID());
		$entrytree = new EntryTreeNodeList($entity,'portfolio_tag_parent');

		echo '<h2>カテゴリ構造のプレビュー</h2>';
		echo '<ul>';
		$entrytree->renderTree('renderRecursiveSimple');
		echo '</ul>';
	}
}
?>
</div>
