<?php
namespace Concrete\Package\CustomRoute;

use Concrete\Core\Package\Package;
use Concrete\Core\Http\Request;
use CustomRoute\CustomRoute;
use Events;
use ReflectionClass;
use Route;
use Environment;
use View;

defined('C5_EXECUTE') or die(_("Access Denied."));

class Controller extends Package
{
    protected $pkgHandle = 'custom_route'; //パッケージハンドル
    protected $appVersionRequired = '5.7.1'; //concrete5のバージョン
    protected $pkgVersion = '1.0.0'; //パッケージのバージョン
    protected $routeID = 'custom_route_id';

    public function getPackageDescription()
    {
        return t("URLに変数を追加");  //パッケージの説明
    }

    public function getPackageName()
    {
        return t("custom_route"); //パッケージ名
    }

    protected $pkgAutoloaderRegistries = array(
        'src/CustomRoute' => '\CustomRoute'
    );

    public function on_start() 
    {   
        //override route
        Route::register('/ccm/system/page/check_in/{cID}/{token}', '\Concrete\Package\CustomRoute\Controller\Backend\Page::exitEditMode');
        Route::register('/ccm/system/panels/page/check_in', '\Concrete\Package\CustomRoute\Controller\Panel\Page\CheckIn::__construct');
        Route::register('/ccm/system/panels/page/check_in/submit', '\Concrete\Package\CustomRoute\Controller\Panel\Page\CheckIn::submit');
        //override views
        $env = Environment::get();
        $env->overrideCoreByPackage('views/panels/page/check_in.php', $this);

        CustomRoute::register('/portfolio/detail/{'.$routeID.'}');
        CustomRoute::register('/staff/detail/{'.$routeID.'}');

        Events::addListener('on_before_dispatch', function() {
            if ($matches = CustomRoute::match()) {

                    // remove placeholders from Request and Globals
                    $request = Request::getInstance();
                    $server_params = $request->server->all();
                    $path = explode('?', $_SERVER['REQUEST_URI'], 2)[0];
                    $server_params['REQUEST_URI'] = str_replace($request->getPathInfo(), '', $path) . $matches['_route']
                        . ($server_params['QUERY_STRING'] != '' ? '?' : '') . $server_params['QUERY_STRING'];
                    $server_params['PHP_SELF'] = rtrim(str_replace($request->getPathInfo(), '', $server_params['PHP_SELF']), '/') . $matches['_route'];
                    $server_params['PATH_TRANSLATED'] = rtrim(str_replace($request->getPathInfo(), '', $server_params['PATH_TRANSLATED']), '/')
                        . $matches['_route'];
                    $server_params['PATH_INFO'] = $matches['_route'];

                    $_SERVER['REQUEST_URI'] = $server_params['REQUEST_URI'];
                    $_SERVER['PATH_INFO'] = $server_params['PATH_INFO'];
                    $_SERVER['PHP_SELF'] = $server_params['PHP_SELF'];
                    $_SERVER['PATH_TRANSLATED'] = $server_params['PATH_TRANSLATED'];

                    $request->server->replace($server_params);

                    $reflectionClass = new ReflectionClass(get_class($request));

                    $property = $reflectionClass->getProperty('pathInfo');
                    $property->setAccessible(true);
                    $property->setValue($request, $server_params['PATH_INFO']);

                    $property = $reflectionClass->getProperty('requestUri');
                    $property->setAccessible(true);
                    $property->setValue($request, $server_params['REQUEST_URI']);

                // add params
                unset($matches['_route']);
                $request->attributes->add($matches);

                foreach($matches as $k => $v) {
                    $_GET[$k] = $v;
                }
            }
        });

        Events::addListener('on_page_view', function() {
            $v = View::getInstance();
            if($_GET['custom_route_id']){
                $v->addHeaderItem('<script type="text/javascript">var CCM_CUSTOMROUTE_ID = '.$_GET['custom_route_id'].';</script>');
            }else{
                $v->addHeaderItem('<script type="text/javascript">var CCM_CUSTOMROUTE_ID = null;</script>');
            }
        });
        
        
    }
}
