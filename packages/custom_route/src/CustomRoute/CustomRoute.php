<?php
namespace CustomRoute;

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Concrete\Core\Http\Request;

defined('C5_EXECUTE') or die(_("Access Denied."));

class CustomRoute {

    protected static $instance;
    protected $routes;

    public function __construct() {
        $this->routes = new RouteCollection();
    }

    public static function getInstance() {
        if (static::$instance === NULL) {
            static::$instance = new static;
        }
        return static::$instance;
    }

    public static function register($uri) {
        if (substr($uri, strlen($uri) - 2) !== '/')
            $uri .= '/';
        static::getInstance()->routes->add($uri, new Route($uri));
    }

    public static function match() {
        $request = Request::getInstance();
        $context = new RequestContext();
        $context->fromRequest($request);

        $matcher = new UrlMatcher(static::getInstance()->routes, $context);

        try {
            $matches = $matcher->match(rtrim($request->getPathInfo(), '/') . '/');

            foreach($matches as $k=>$v) {

                if ($k == '_route') {
                    continue;
                }
                $matches['_route'] = str_replace('/{'.$k.'}','', $matches['_route']);
            }
            $matches['_route'] = rtrim($matches['_route'], '/');

            return $matches;

        } catch (ResourceNotFoundException $e) {

            return null;
        }
    }
}